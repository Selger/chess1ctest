﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pieces;
using Pieces.Pieces;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	public Camera MainCamera;
	public InputController InputController;
	public GameObject PiecesPanel;
	public PiecesHolder PiecesHolder;
	public GameObject Board;
	public GameObject GridElWhite;
	public GameObject GridElBlack;
	public int GridSize = 8;
	public int CellSize = 30;
	public GameObject WinText;

	public Color HighlightedWhite;
	public Color HighlightedBlack;
	public Color NormalWhite;
	public Color NormalBlack;

	private GameObject[,] _gridEls;
	private readonly List<IPiece> _pieces = new List<IPiece>();
	private GameObject[,] _piecesObjects;

	private bool _isWhiteTurn = true;

	// Use this for initialization
	void Start()
	{
		_gridEls = new GameObject[GridSize, GridSize];
		_piecesObjects = new GameObject[GridSize,GridSize];
		GenerateGrid();
		SpawnPieces();

		InputController.BeginDrag += HighlightMoves;
		InputController.Drag += MovePiece;
		InputController.EndDrag += ChangePieceCell;
	}

	private void GenerateGrid()
	{
		var isWhity = false;
		for (var i = 0; i < GridSize; i++)
		{
			for (var j = 0; j < GridSize; j++)
			{
				if (j == 0)
				{
					isWhity = !isWhity;
				}
				isWhity = !isWhity;
				var gridEl = Instantiate(isWhity ? GridElWhite : GridElBlack);
				gridEl.GetComponent<Image>().color = isWhity ? NormalWhite : NormalBlack;
				gridEl.GetComponent<BoardElement>().Position = new Vector2(i, j);
				gridEl.GetComponent<BoardElement>().IsWhite = isWhity;

				var newTransform = gridEl.GetComponent<RectTransform>();
				newTransform.anchorMin = new Vector2(0, 1);
				newTransform.anchorMax = new Vector2(0, 1);
				newTransform.pivot = new Vector2(0, 1);
				newTransform.anchoredPosition = new Vector3(i * CellSize, -j * CellSize, 0);
				newTransform.sizeDelta = new Vector2(CellSize, CellSize);
				newTransform.SetParent(transform, false);

				_gridEls[i, j] = gridEl;
			}
		}
	}

	private void SpawnPieces()
	{
		//spawn pawn
		for (var i = 0; i <= 7; i++)
		{
			_pieces.Add(SpawnPiece(new Vector2(1, i), PiecesHolder.BlackPawn, false, typeof(BlackPawn)));
			_pieces.Add(SpawnPiece(new Vector2(6, i), PiecesHolder.WhitePawn, true, typeof(WhitePawn)));
		}
		//spawn rooks
		SpawnPiece(new Vector2(0, 0), PiecesHolder.BlackRook, false, typeof(Rook));
		SpawnPiece(new Vector2(0, 7), PiecesHolder.BlackRook, false, typeof(Rook));
		SpawnPiece(new Vector2(7, 0), PiecesHolder.WhiteRook, true, typeof(Rook));
		SpawnPiece(new Vector2(7, 7), PiecesHolder.WhiteRook, true, typeof(Rook));
		//spawn bioshops
		SpawnPiece(new Vector2(0, 2), PiecesHolder.BlackBishop, false, typeof(Bishop));
		SpawnPiece(new Vector2(0, 5), PiecesHolder.BlackBishop, false, typeof(Bishop));
		SpawnPiece(new Vector2(7, 2), PiecesHolder.WhiteBishop, true, typeof(Bishop));
		SpawnPiece(new Vector2(7, 5), PiecesHolder.WhiteBishop, true, typeof(Bishop));
		//spawn kings
		SpawnPiece(new Vector2(0, 4), PiecesHolder.BlackKing, false, typeof(King));
		SpawnPiece(new Vector2(7, 4), PiecesHolder.WhiteKing, true, typeof(King));
		//spawn queens
		SpawnPiece(new Vector2(0, 3), PiecesHolder.BlackQueen, false, typeof(Queen));
		SpawnPiece(new Vector2(7, 3), PiecesHolder.WhiteQueen, true, typeof(Queen));
		//spawn knights
		SpawnPiece(new Vector2(0, 1), PiecesHolder.BlackKnight, false, typeof(Knight));
		SpawnPiece(new Vector2(0, 6), PiecesHolder.BlackKnight, false, typeof(Knight));
		SpawnPiece(new Vector2(7, 1), PiecesHolder.WhiteKnight, true, typeof(Knight));
		SpawnPiece(new Vector2(7, 6), PiecesHolder.WhiteKnight, true, typeof(Knight));
	}

	private IPiece SpawnPiece(Vector2 inGameCoord, GameObject prefab, bool isWhite, Type pieceType)
	{
		var piece = Instantiate(prefab);

		var tm = piece.transform as RectTransform;
		tm.sizeDelta = new Vector2(CellSize, CellSize);
		FitPieceToCell(piece, inGameCoord);

		piece.AddComponent<Selectable>();
		var pieceComponent = piece.AddComponent(pieceType) as IPiece;

		pieceComponent.SetNewPosition(inGameCoord);
		pieceComponent.Init(GridSize, isWhite, this);

		_pieces.Add(pieceComponent);
		_piecesObjects[(int) inGameCoord.x, (int) inGameCoord.y] = piece;
		return pieceComponent;
	}

	private void UnHighlightMoves(List<Vector2> moves)
	{
		foreach (var move in moves)
		{
			var cell = GetCellAtPosition(move);
			UnHighlightCell(cell);
		}
	}

	private void UnHighlightCell(GameObject cell)
	{
		var image = cell.GetComponent<Image>();
		image.color = cell.GetComponent<BoardElement>().IsWhite ? NormalWhite : NormalBlack;
	}

	private void HighlightMoves(GameObject piece)
	{
		if (piece.GetComponent<IPiece>().IsInWhiteTime() == _isWhiteTurn)
		{
			var piceController = piece.GetComponent<IPiece>();
			HighlightMoves(piceController.GetAvailableMoves());
		}
	}

	private void HighlightMoves(List<Vector2> moves)
	{
		foreach (var move in moves)
		{
			var cell = GetCellAtPosition(move);
			HighlightCell(cell);
		}
	}

	private void HighlightCell(GameObject cell)
	{
		var image = cell.GetComponent<Image>();
		image.color = cell.GetComponent<BoardElement>().IsWhite ? HighlightedWhite : HighlightedBlack;
	}

	private GameObject GetCellAtPosition(Vector2 position)
	{
		return _gridEls[(int) position.x, (int) position.y];
	}

	public IPiece GetPieceAtPosition(Vector2 position)
	{
		return _pieces.FirstOrDefault(p => p.GetCurrentPosition() == position);
	}

	private void RemovePiece(IPiece targetPiece, Vector2 position)
	{
		var king = targetPiece as King;
		if (king != null)
		{
			if (king.IsInWhiteTime())
			{
				WinText.SetActive(true);
				//Black Win!
			}
			else
			{
				WinText.GetComponent<Text>().text = "White wins!";
				WinText.SetActive(true);
				//White Win!
			}
			PiecesPanel.SetActive(false);
			Board.SetActive(false);
		}
		_pieces.Remove(targetPiece);
		Destroy(_piecesObjects[(int)position.x, (int)position.y]);
	}

	private void TryEat(Vector2 position)
	{
		var targetPiece = GetPieceAtPosition(position);
		if (targetPiece == null)
		{
			return;
		}
		RemovePiece(targetPiece, position);
	}

	private void MovePiece(GameObject piece)
	{
		if (piece.GetComponent<IPiece>().IsInWhiteTime() == _isWhiteTurn)
		{
			var trans = piece.GetComponent<RectTransform>();
			var position = MainCamera.ScreenToWorldPoint(Input.mousePosition);
			trans.position = new Vector3(position.x, position.y, 0);
		}
	}

	private void ChangePieceCell(GameObject piece, GameObject newCell)
	{
		if (piece.GetComponent<IPiece>().IsInWhiteTime() == _isWhiteTurn)
		{
			var pieceController = piece.gameObject.GetComponent<IPiece>();
			UnHighlightMoves(pieceController.GetAvailableMoves());

			if(newCell == null || newCell.GetComponent<BoardElement>() == null)
			{
				FitPieceToCell(piece, pieceController.GetCurrentPosition());
				return;
			}

			var newPosition = newCell.GetComponent<BoardElement>().Position;
			if (pieceController.IsMoveValid(newPosition))
			{
				//Cell is free, let's put piece on it.
				TryEat(newPosition);
				FitPieceToCell(piece, newCell);
				pieceController.SetNewPosition(newPosition);
				_isWhiteTurn = !_isWhiteTurn;
			}
			else
			{
				FitPieceToCell(piece, pieceController.GetCurrentPosition());
			}
		}
	}

	private void FitPieceToCell(GameObject piece, Vector2 cellPosition)
	{
		var cell = _gridEls[(int) cellPosition.x, (int) cellPosition.y];
		FitPieceToCell(piece, cell);
	}

	private void FitPieceToCell(GameObject piece, GameObject cell)
	{
		var tm = piece.GetComponent<RectTransform>();
		tm.SetParent(cell.gameObject.transform, false);
		tm.anchorMin = new Vector2(0.5f, 0.5f);
		tm.anchorMax = new Vector2(0.5f, 0.5f);
		tm.pivot = new Vector2(0.5f, 0.5f);
		tm.anchoredPosition = new Vector2(0, 0);
		tm.SetParent(PiecesPanel.transform);
	}
}
