﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiecesHolder : MonoBehaviour
{
	[Header("Black Team")]
	public GameObject BlackKnight;
	public GameObject BlackKing;
	public GameObject BlackBishop;
	public GameObject BlackPawn;
	public GameObject BlackQueen;
	public GameObject BlackRook;

	[Header("White Team")]
	public GameObject WhiteKnight;
	public GameObject WhiteKing;
	public GameObject WhiteBishop;
	public GameObject WhitePawn;
	public GameObject WhiteQueen;
	public GameObject WhiteRook;

}
