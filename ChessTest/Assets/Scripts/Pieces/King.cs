﻿using System.Collections.Generic;
using Pieces;
using Pieces.Pieces;
using UnityEngine;

public class King : Piece {
    protected override void CalculateAvailableMoves()
    {
        AvailableMoves.Clear();
        if (CurrentPosition.x + 1 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x + 1 < GridSize && CurrentPosition.y + 1 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y + 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x + 1 < GridSize && CurrentPosition.y - 1 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y - 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.y - 1 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x, CurrentPosition.y - 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x - 1 >= 0 && CurrentPosition.y -1 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x - 1, CurrentPosition.y - 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x - 1 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x - 1, CurrentPosition.y);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x - 1 >= 0 && CurrentPosition.y + 1 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x - 1, CurrentPosition.y + 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.y + 1 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x, CurrentPosition.y + 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
    }
}
