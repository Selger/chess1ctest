﻿using UnityEngine;

namespace Pieces.Pieces
{
    public class BlackPawn : Piece
    {
        protected override void CalculateAvailableMoves()
        {
            AvailableMoves.Clear();
            if (CurrentPosition.x + 1 < GridSize)
            {
                var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y);
                if (MoveHelper.FilterMove(newPos, GameController))
                {
                    AvailableMoves.Add(newPos);
                }
            }
            if (CurrentPosition.x + 1 < GridSize && CurrentPosition.y - 1 >= 0)
            {
                var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y - 1);
                if (MoveHelper.FilterEat(newPos, this ,GameController))
                {
                    AvailableMoves.Add(newPos);
                }
            }
            if (CurrentPosition.x + 1 < GridSize && CurrentPosition.y + 1 < GridSize)
            {
                var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y + 1);
                if (MoveHelper.FilterEat(newPos, this ,GameController))
                {
                    AvailableMoves.Add(newPos);
                }
            }
        }
    }
}