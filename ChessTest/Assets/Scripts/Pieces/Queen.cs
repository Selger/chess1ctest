﻿using Pieces;
using Pieces.Pieces;
using UnityEngine;

public class Queen : Piece {
	protected override void CalculateAvailableMoves()
	{
		AvailableMoves.Clear();
		for (var i = 1; CurrentPosition.x + i < GridSize && CurrentPosition.y + i < GridSize; i++)
		{
			var newPos = new Vector2(CurrentPosition.x + i, CurrentPosition.y + i);
			if (TryMove(newPos))
			{
				break;
			}
		}
		for (var i = 1; CurrentPosition.x + i < GridSize && CurrentPosition.y - i >= 0; i++)
		{
			var newPos = new Vector2(CurrentPosition.x + i, CurrentPosition.y - i);
			if (TryMove(newPos))
			{
				break;
			}
		}
		for (var i = 1; CurrentPosition.x - i >= 0 && CurrentPosition.y - i >= 0; i++)
		{
			var newPos = new Vector2(CurrentPosition.x - i, CurrentPosition.y - i);
			if (TryMove(newPos))
			{
				break;
			}
		}
		for (var i = 1; CurrentPosition.x - i >= 0 && CurrentPosition.y + i < GridSize; i++)
		{
			var newPos = new Vector2(CurrentPosition.x - i, CurrentPosition.y + i);
			if (TryMove(newPos))
			{
				break;
			}
		}
		for (var i = 1; CurrentPosition.y - i >= 0; i++)
		{
			var newPos = new Vector2(CurrentPosition.x , CurrentPosition.y - i);
			if (TryMove(newPos))
			{
				break;
			}
		}
		for (var i = 1; CurrentPosition.y + i < GridSize; i++)
		{
			var newPos = new Vector2(CurrentPosition.x, CurrentPosition.y + i);
			if (TryMove(newPos))
			{
				break;
			}
		}
		for (var i = 1; CurrentPosition.x + i < GridSize; i++)
		{
			var newPos = new Vector2(CurrentPosition.x + i, CurrentPosition.y);
			if (TryMove(newPos))
			{
				break;
			}
		}
		for (var i = 1; CurrentPosition.x - i >= 0; i++)
		{
			var newPos = new Vector2(CurrentPosition.x - i, CurrentPosition.y);
			if (TryMove(newPos))
			{
				break;
			}
		}
	}

	private bool TryMove(Vector2 newPos)
	{
		if(MoveHelper.FilterMove(newPos, GameController))
		{
			AvailableMoves.Add(newPos);
			return false;
		}
		if(MoveHelper.FilterEat(newPos, this, GameController))
		{
			AvailableMoves.Add(newPos);
			return true;
		}
		return true;
	}
}
