﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pieces.Pieces
{
    public abstract class Piece : MonoBehaviour, IPiece
    {
        protected abstract void CalculateAvailableMoves();

        protected GameController GameController;
        protected Vector2 CurrentPosition;
        protected readonly List<Vector2> AvailableMoves = new List<Vector2>();
        protected int GridSize;
        private bool _isInWhiteTeam;

        public bool IsInWhiteTime()
        {
            return _isInWhiteTeam;
        }

        public void Init(int gridSize, bool isWhiteTeam, GameController controller)
        {
            GridSize = gridSize;
            _isInWhiteTeam = isWhiteTeam;
            GameController = controller;
        }

        public bool IsMoveValid(Vector2 move)
        {
            return AvailableMoves.FirstOrDefault(m => m == move) != Vector2.zero;
        }

        public List<Vector2> GetAvailableMoves()
        {
            CalculateAvailableMoves();
            return AvailableMoves;
        }

        public Vector2 GetCurrentPosition()
        {
            return CurrentPosition;
        }

        public void SetNewPosition(Vector2 newPosition)
        {
            CurrentPosition = newPosition;
        }
    }
}