﻿using System.Collections;
using System.Collections.Generic;
using Pieces;
using Pieces.Pieces;
using UnityEngine;

public class Knight : Piece {
    protected override void CalculateAvailableMoves()
    {
        AvailableMoves.Clear();
        if (CurrentPosition.x + 2 < GridSize && CurrentPosition.y - 1 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x + 2, CurrentPosition.y - 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x + 2 < GridSize && CurrentPosition.y + 1 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x + 2, CurrentPosition.y + 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x + 1 < GridSize && CurrentPosition.y + 2 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y + 2);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x - 1 >= 0 && CurrentPosition.y + 2 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x - 1, CurrentPosition.y + 2);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x - 2 >= 0 && CurrentPosition.y + 1 < GridSize)
        {
            var newPos = new Vector2(CurrentPosition.x - 2, CurrentPosition.y + 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x - 2 >= 0 && CurrentPosition.y - 1 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x - 2, CurrentPosition.y - 1);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x - 1 >= 0 && CurrentPosition.y - 2 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x - 1, CurrentPosition.y - 2);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
        if (CurrentPosition.x + 1 < GridSize && CurrentPosition.y - 2 >= 0)
        {
            var newPos = new Vector2(CurrentPosition.x + 1, CurrentPosition.y - 2);
            if (MoveHelper.FilterMoveAndEat(newPos, this ,GameController))
            {
                AvailableMoves.Add(newPos);
            }
        }
    }
}
