﻿using UnityEngine;

public class BoardElement : MonoBehaviour
{
    public Vector2 Position;
    public bool IsWhite;
}
