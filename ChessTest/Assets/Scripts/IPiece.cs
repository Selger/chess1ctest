﻿using System.Collections.Generic;
using UnityEngine;

public interface IPiece
{
    bool IsInWhiteTime();
    void Init(int gridSize, bool isWhiteTeam, GameController controller);
    bool IsMoveValid(Vector2 move);
    List<Vector2> GetAvailableMoves();
    Vector2 GetCurrentPosition();
    void SetNewPosition(Vector2 newPosition);
}