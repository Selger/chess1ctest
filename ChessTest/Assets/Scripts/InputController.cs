﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputController : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{

	public GraphicRaycaster Raycaster;

	public delegate void BeginDragCallback(GameObject piece);

	public delegate void DragCallback(GameObject piece);

	public delegate void DragEndCallback(GameObject piece, GameObject cell);

	public event BeginDragCallback BeginDrag;
	public event DragCallback Drag;
	public event DragEndCallback EndDrag;

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (eventData.selectedObject != null && eventData.selectedObject.tag.Equals("Piece") &&
		    BeginDrag != null)
		{
			BeginDrag(eventData.selectedObject);
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (eventData.selectedObject != null && eventData.selectedObject.tag.Equals("Piece") &&
		    Drag != null)
		{
			Drag(eventData.selectedObject);
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (eventData.selectedObject != null && eventData.selectedObject.tag.Equals("Piece") &&
		    EndDrag != null)
		{
			var ped = new PointerEventData(null) {position = eventData.position};
			var results = new List<RaycastResult>();
			Raycaster.Raycast(ped, results);

			GameObject boardEl = null;
			foreach (var result in results)
			{
				if (result.gameObject.tag.Equals("Cell"))
				{
					boardEl = result.gameObject;
				}
			}

			EndDrag(eventData.selectedObject, boardEl);
		}
	}
}
