﻿using System.Collections.Generic;
using UnityEngine;

namespace Pieces
{
    public static class MoveHelper
    {
        public static bool FilterMoveAndEat(Vector2 move, IPiece piece, GameController controller)
        {
            var targetPiece = controller.GetPieceAtPosition(move);
            return targetPiece == null || IsEnemy(piece, targetPiece);
        }

        public static bool FilterMove(Vector2 move, GameController controller)
        {
            var targetPiece = controller.GetPieceAtPosition(move);
            return targetPiece == null;
        }

        public static bool FilterEat(Vector2 move, IPiece piece, GameController controller)
        {
            var targetPiece = controller.GetPieceAtPosition(move);
            return targetPiece != null && IsEnemy(piece, targetPiece);
        }

        private static bool IsEnemy(IPiece original, IPiece opponent)
        {
            return original.IsInWhiteTime() ^ opponent.IsInWhiteTime();
        }
    }
}